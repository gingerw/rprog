set.seed(10)
x <- rnorm(100)
# x <- rbinom(100, 1, 0.5)
e <- rnorm(100, 0, 2)
y <- 0.5 + 2 * x + e
summary(y)
plot(x, y)

# assignment 3 warm up
setwd("/Users/garlic/workspace/R/rprog/ass3")

outcome <- read.csv("data/outcome-of-care-measures.csv", colClasses = "character")
# head(outcome)
# ncol(outcome)
# names(outcome)

outcome[, 11] <- as.numeric(outcome[, 11])
hist(outcome[, 11])
