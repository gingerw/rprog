setwd("/Users/garlic/workspace/R/rprog/ass1")

pollutantmean <- function(directory, pollutant, id = 1:332) {
  ## 'directory' is a character vector of length 1 indicating
  ## the location of the CSV files
  
  files <- list.files(
    path=directory, 
    pattern=".csv",
    full.names = T)
  #print(files)
  df <- do.call(rbind, lapply(files[id], read.csv))
  #print(df)
  #str(df)
  #summary(df)
  names(df)
  
  ## 'pollutant' is a character vector of length 1 indicating
  ## the name of the pollutant for which we will calculate the
  ## mean; either "sulfate" or "nitrate".
  
  ## 'id' is an integer vector indicating the monitor ID numbers
  ## to be used
  
  ## Return the mean of the pollutant across all monitors list
  ## in the 'id' vector (ignoring NA values)
  ## NOTE: Do not round the result!
  
  mean(
    #df[which(df[, "ID"] >= min(id) & df$ID <= max(id)) , pollutant],
    df[which(df[, "ID"] %in% id) , pollutant],
    na.rm = T
  )
}

## [1] 4.064
pollutantmean("specdata", "sulfate", 1:10)

## [1] 1.706
pollutantmean("specdata", "nitrate", 70:72)

## [1] 1.281
pollutantmean("specdata", "nitrate", 23)